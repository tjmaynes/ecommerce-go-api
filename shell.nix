with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "ecommerce-go-api";
  buildInputs = [
    go
    postgresql
  ];
}
