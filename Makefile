ENVIRONMENT := development

include .env.$(ENVIRONMENT)
export $(shell sed 's/=.*//' .env.$(ENVIRONMENT))
export TAG=$(shell git rev-parse --short HEAD)

install:
	./scripts/install.sh

generate_mocks:
	moq -out internal/shared/product/repository_mock.go internal/shared/product Repository

seed:
	go run ./seeder \
		--db-source=$(DATABASE_URL) \
		--item-count=100 \
		--manufacturer-count=5

migrate:
	DATABASE_URL=$(DATABASE_URL) bin/dbmate wait
	DATABASE_URL=$(DATABASE_URL) bin/dbmate up

test: migrate generate_mocks
	DATABASE_URL=$(DATABASE_URL) \
	PORT=$(PORT) \
	go test -v -coverprofile=coverage.txt ./...

ci_test:
	make test 2>&1 | go-junit-report > report.xml
	gocov convert coverage.txt > coverage.json
	gocov-xml < coverage.json > coverage.xml
	(mkdir -p coverage || true) && gocov-html < coverage.json > coverage/index.html

build:
	go build -o dist/ecommerce ./ecommerce

start: build migrate
	DATABASE_URL=$(DATABASE_URL) \
	PORT=$(PORT) \
	./dist/ecommerce

build_image:
	./scripts/build-image.sh

push_image:
	./scripts/push-image.sh

debug_image:
	./scripts/debug-image.sh

deploy_local_k8s:
	./scripts/deploy-k8s.sh "local"

deploy_gke_k8s:
	./scripts/deploy-k8s.sh "gke"

deploy: test build_image push_image

connect_localhost_to_remote_db:
	kubectl port-forward svc/ecommerce-go-api-db 5432:5432

init_local_db:
	./scripts/init-db.sh

debug_local_db:
	kubectl exec -it ecommerce-go-api-db-99897676c-hkntq -- psql \
		--username $(POSTGRES_USER) \
		--password $(POSTGRES_PASS) \
		$(POSTGRES_DB)

destroy_local_k8s:
	./scripts/destroy-k8s.sh "local"

destroy_gke_k8s:
	./scripts/destroy-k8s.sh "gke"

clean:
	rm -rf dist/ vendor/ coverage* report.xml
