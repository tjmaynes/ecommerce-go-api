package product

import (
	"context"
	"errors"
	"testing"

	"github.com/icrowley/fake"
)

func Test_Product_Service_GetItems_WhenItemsExist_ShouldReturnAllItems(t *testing.T) {
	items := []ProductItem{
		{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()},
		{ID: 2, Name: fake.ProductName(), Price: 4, Manufacturer: fake.Brand()},
		{ID: 3, Name: fake.ProductName(), Price: 5, Manufacturer: fake.Brand()},
		{ID: 4, Name: fake.ProductName(), Price: 11, Manufacturer: fake.Brand()},
		{ID: 5, Name: fake.ProductName(), Price: 100, Manufacturer: fake.Brand()},
	}

	const pageSize = 10
	const page = 0
	var pageSizeCalled int64
	var pageCalled int64

	mockRepository := &RepositoryMock{
		GetProductsFunc: func(ctx context.Context, pageSize int64, page int64) ([]ProductItem, error) {
			pageSizeCalled = pageSize
			pageCalled = page
			return items, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	results, err := sut.GetProducts(ctx, pageSize, page)
	if err != nil {
		t.Fatalf("Should not have failed!")
	}

	if len(results) != len(items) {
		t.Errorf("Expected an array of product items of size %d. Got %d", len(items), len(results))
	}

	callsToSend := len(mockRepository.GetProductsCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}

	if pageSizeCalled != pageSize {
		t.Errorf("Unexpected recipient: %d", pageSizeCalled)
	}

	if pageCalled != page {
		t.Errorf("Unexpected recipient: %d", pageCalled)
	}
}

func Test_Product_Service_GetItemByID_WhenItemExists_ShouldReturnItem(t *testing.T) {
	item := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}
	const id = 10
	var idCalled int64

	mockRepository := &RepositoryMock{
		GetProductByIDFunc: func(ctx context.Context, id int64) (ProductItem, error) {
			idCalled = id
			return item, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	result, err := sut.GetProductByID(ctx, id)
	if err != nil {
		t.Fatalf("Should not have failed!")
	}

	if result != item {
		t.Errorf("Expected product items %+v. Got %+v", item, result)
	}

	callsToSend := len(mockRepository.GetProductByIDCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}

	if idCalled != id {
		t.Errorf("Unexpected recipient: %d", id)
	}
}

func Test_Product_Service_GetItemByID_WhenItemDoesNotExist_ShouldReturnError(t *testing.T) {
	testError := createError()

	mockRepository := &RepositoryMock{
		GetProductByIDFunc: func(ctx context.Context, id int64) (ProductItem, error) {
			return ProductItem{}, testError
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	_, err := sut.GetProductByID(ctx, 0)
	if err != testError {
		t.Errorf("Expected error message %s. Got %s", testError, err)
	}

	callsToSend := len(mockRepository.GetProductByIDCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_AddItem_WhenGivenValidItem_ShouldReturnItem(t *testing.T) {
	var itemCalled *ProductItem
	newItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(99),
		Manufacturer: fake.Brand(),
	}

	mockRepository := &RepositoryMock{
		AddProductFunc: func(ctx context.Context, item *ProductItem) (ProductItem, error) {
			itemCalled = &newItem
			return newItem, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	result, err := sut.AddProduct(ctx, newItem.Name, newItem.Price, newItem.Manufacturer)
	if err != nil {
		t.Fatalf("Should not have failed!")
	}

	if result != *itemCalled {
		t.Errorf("Expected product item: %+v. Got %+v", itemCalled, result)
	}

	callsToSend := len(mockRepository.AddProductCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_AddItem_WhenGivenInvalidItem_ShouldReturnError(t *testing.T) {
	newItem := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}

	mockRepository := &RepositoryMock{
		AddProductFunc: func(ctx context.Context, item *ProductItem) (ProductItem, error) {
			return ProductItem{}, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	expectedErrorMessage := "price: must be no less than 99."

	_, err := sut.AddProduct(ctx, newItem.Name, newItem.Price, newItem.Manufacturer)
	if err.Error() != expectedErrorMessage {
		t.Errorf("Error unexpected error message %s was given", err)
	}

	callsToSend := len(mockRepository.AddProductCalls())
	if callsToSend != 0 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_UpdateProduct_WhenGivenValidItem_ShouldReturnItem(t *testing.T) {
	var itemCalled *ProductItem

	updatedItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(99),
		Manufacturer: fake.Brand(),
	}

	mockRepository := &RepositoryMock{
		UpdateProductFunc: func(ctx context.Context, item *ProductItem) (ProductItem, error) {
			itemCalled = &updatedItem
			return updatedItem, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	result, err := sut.UpdateProduct(ctx, updatedItem.ID, updatedItem.Name, updatedItem.Price, updatedItem.Manufacturer)
	if err != nil {
		t.Fatalf("Should not have failed!")
	}

	if result != *itemCalled {
		t.Errorf("Expected product item: %+v. Got %+v", itemCalled, result)
	}

	callsToSend := len(mockRepository.UpdateProductCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_UpdateProduct_WhenGivenInvalidItem_ShouldReturnServiceError(t *testing.T) {
	invalidItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(25),
		Manufacturer: fake.Brand(),
	}

	mockRepository := &RepositoryMock{
		UpdateProductFunc: func(ctx context.Context, item *ProductItem) (ProductItem, error) {
			return ProductItem{}, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	_, serviceError := sut.UpdateProduct(ctx, invalidItem.ID, invalidItem.Name, invalidItem.Price, invalidItem.Manufacturer)

	if serviceError.StatusCode() != InvalidItem {
		t.Errorf("Error unexpected error message %s was given", serviceError.Message())
	}

	callsToSend := len(mockRepository.UpdateProductCalls())
	if callsToSend != 0 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_UpdateProduct_WhenUnknownErrorOccurs_ShouldReturnServiceError(t *testing.T) {
	invalidItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(99),
		Manufacturer: fake.Brand(),
	}

	err := errors.New("Unknown error occurred")

	mockRepository := &RepositoryMock{
		UpdateProductFunc: func(ctx context.Context, item *ProductItem) (ProductItem, error) {
			return ProductItem{}, err
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	_, serviceError := sut.UpdateProduct(ctx, invalidItem.ID, invalidItem.Name, invalidItem.Price, invalidItem.Manufacturer)

	if serviceError.StatusCode() != UnknownException {
		t.Errorf("Error unexpected error message %s was given", serviceError.Message())
	}

	callsToSend := len(mockRepository.UpdateProductCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_RemoveProduct_WhenItemExists_ShouldReturnItemID(t *testing.T) {
	var idCalled int64
	deletedItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(99),
		Manufacturer: fake.Brand(),
	}

	mockRepository := &RepositoryMock{
		RemoveProductFunc: func(ctx context.Context, id int64) (int64, error) {
			idCalled = deletedItem.ID
			return deletedItem.ID, nil
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	result, err := sut.RemoveProduct(ctx, deletedItem.ID)
	if err != nil {
		t.Fatalf("Should not have failed!")
	}

	if result != idCalled {
		t.Errorf("Expected product item: %d. Got %d", idCalled, result)
	}

	callsToSend := len(mockRepository.RemoveProductCalls())
	if callsToSend != 1 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}

func Test_Product_Service_RemoveProduct_WhenUnknownErrorOccurs_ShouldReturnServiceError(t *testing.T) {
	deletedItem := ProductItem{
		ID:           int64(1),
		Name:         fake.ProductName(),
		Price:        Decimal(99),
		Manufacturer: fake.Brand(),
	}

	unknownError := errors.New("Unknown Error")

	mockRepository := &RepositoryMock{
		RemoveProductFunc: func(ctx context.Context, id int64) (int64, error) {
			return deletedItem.ID, unknownError
		},
	}

	ctx := context.Background()
	sut := NewService(mockRepository)

	_, serviceError := sut.RemoveProduct(ctx, deletedItem.ID)
	if serviceError.StatusCode() != UnknownException {
		t.Errorf("Error unexpected error message %s was given", serviceError.Message())
	}

	callsToSend := len(mockRepository.UpdateProductCalls())
	if callsToSend != 0 {
		t.Errorf("Send was called %d times", callsToSend)
	}
}
