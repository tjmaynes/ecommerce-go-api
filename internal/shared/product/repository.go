package product

import (
	"context"
	"database/sql"
)

// Repository ..
type Repository interface {
	GetProducts(ctx context.Context, page int64, pageSize int64) ([]ProductItem, error)
	GetProductByID(ctx context.Context, id int64) (ProductItem, error)
	AddProduct(ctx context.Context, item *ProductItem) (ProductItem, error)
	UpdateProduct(ctx context.Context, item *ProductItem) (ProductItem, error)
	RemoveProduct(ctx context.Context, id int64) (int64, error)
}

// NewRepository ..
func NewRepository(DBConn *sql.DB) Repository {
	return &repository{DBConn: DBConn}
}

// repository ..
type repository struct {
	DBConn *sql.DB
}

// GetProducts ..
func (r *repository) GetProducts(ctx context.Context, page int64, pageSize int64) ([]ProductItem, error) {
	limit := pageSize
	offset := page * pageSize

	rows, err := r.DBConn.QueryContext(ctx, "SELECT id, name, price, manufacturer FROM product ORDER BY id LIMIT $1 OFFSET $2", limit, offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	payload := make([]ProductItem, 0)
	for rows.Next() {
		data := new(ProductItem)
		err := rows.Scan(&data.ID, &data.Name, &data.Price, &data.Manufacturer)
		if err != nil {
			return nil, err
		}
		payload = append(payload, *data)
	}

	return payload, nil
}

// GetProductByID ..
func (r *repository) GetProductByID(ctx context.Context, id int64) (ProductItem, error) {
	var item ProductItem
	row := r.DBConn.QueryRowContext(ctx, "SELECT id, name, price, manufacturer FROM product WHERE id = $1", id)
	err := row.Scan(&item.ID, &item.Name, &item.Price, &item.Manufacturer)
	if err != nil {
		return ProductItem{}, err
	}

	return item, nil
}

// AddProduct ..
func (r *repository) AddProduct(ctx context.Context, item *ProductItem) (ProductItem, error) {
	var insertedID int64
	insertStm := "INSERT INTO product (name, price, manufacturer) VALUES ($1, $2, $3) RETURNING ID"
	err := r.DBConn.QueryRowContext(ctx, insertStm, item.Name, item.Price, item.Manufacturer).Scan(&insertedID)
	if err != nil {
		return ProductItem{}, err
	}

	return ProductItem{
		ID:           insertedID,
		Name:         item.Name,
		Price:        item.Price,
		Manufacturer: item.Manufacturer,
	}, nil
}

// UpdateProduct ..
func (r *repository) UpdateProduct(ctx context.Context, item *ProductItem) (ProductItem, error) {
	tx, err := r.DBConn.BeginTx(ctx, nil)
	if err != nil {
		return ProductItem{}, err
	}

	_, err = tx.ExecContext(ctx, "UPDATE product SET name = $1, price = $2, manufacturer = $3 WHERE id = $4", item.Name, item.Price, item.Manufacturer, item.ID)
	if err != nil {
		tx.Rollback()
		return ProductItem{}, err
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return ProductItem{}, err
	}

	return *item, nil
}

// RemoveProduct ..
func (r *repository) RemoveProduct(ctx context.Context, id int64) (int64, error) {
	stmt, err := r.DBConn.PrepareContext(ctx, "DELETE FROM product WHERE id = $1")
	if err != nil {
		return id, err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return id, err
	}

	return id, nil
}
