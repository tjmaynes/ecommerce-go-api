package product

import (
	"context"
)

// Service ..
type Service interface {
	GetProducts(ctx context.Context, page int64, pageSize int64) ([]ProductItem, error)
	GetProductByID(ctx context.Context, id int64) (ProductItem, error)
	AddProduct(
		ctx context.Context,
		name string,
		price Decimal,
		manufacturer string,
	) (ProductItem, error)
	UpdateProduct(
		ctx context.Context,
		id int64,
		name string,
		price Decimal,
		manufacturer string,
	) (ProductItem, ServiceError)
	RemoveProduct(ctx context.Context, id int64) (int64, ServiceError)
}

// NewService ..
func NewService(repository Repository) Service {
	return &service{
		Repository: repository,
	}
}

type service struct {
	Repository Repository
}

// GetProducts ..
func (s *service) GetProducts(ctx context.Context, page int64, pageSize int64) ([]ProductItem, error) {
	return s.Repository.GetProducts(ctx, page, pageSize)
}

// GetProductByID ..
func (s *service) GetProductByID(ctx context.Context, id int64) (ProductItem, error) {
	return s.Repository.GetProductByID(ctx, id)
}

// AddProduct ..
func (s *service) AddProduct(ctx context.Context, name string, price Decimal, manufacturer string) (ProductItem, error) {
	item := ProductItem{Name: name, Price: price, Manufacturer: manufacturer}

	err := item.Validate()
	if err != nil {
		return ProductItem{}, err
	}

	return s.Repository.AddProduct(ctx, &item)
}

// UpdateProduct ..
func (s *service) UpdateProduct(ctx context.Context, id int64, name string, price Decimal, manufacturer string) (ProductItem, ServiceError) {
	item := ProductItem{ID: id, Name: name, Price: price, Manufacturer: manufacturer}

	err := item.Validate()
	if err != nil {
		return ProductItem{}, CreateServiceError(err.Error(), InvalidItem)
	}

	result, err := s.Repository.UpdateProduct(ctx, &item)
	if err != nil {
		return ProductItem{}, CreateServiceError(err.Error(), UnknownException)
	}

	return result, nil
}

// RemoveProduct ..
func (s *service) RemoveProduct(ctx context.Context, id int64) (int64, ServiceError) {
	result, err := s.Repository.RemoveProduct(ctx, id)
	if err != nil {
		return id, CreateServiceError(err.Error(), UnknownException)
	}

	return result, nil
}
