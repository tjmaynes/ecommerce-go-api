package product

import (
	"context"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/icrowley/fake"
)

func Test_ProductRepository_GetProducts_ShouldReturnProducts(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	const pageSize = 5
	const page = 0

	columns := []string{"id", "name", "price", "manufacturer"}
	item1 := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}
	item2 := ProductItem{ID: 2, Name: fake.ProductName(), Price: 4, Manufacturer: fake.Brand()}
	item3 := ProductItem{ID: 3, Name: fake.ProductName(), Price: 5, Manufacturer: fake.Brand()}
	item4 := ProductItem{ID: 4, Name: fake.ProductName(), Price: 11, Manufacturer: fake.Brand()}
	item5 := ProductItem{ID: 5, Name: fake.ProductName(), Price: 100, Manufacturer: fake.Brand()}

	mock.ExpectQuery("SELECT id, name, price, manufacturer FROM product ORDER BY id LIMIT \\$1 OFFSET \\$2").
		WithArgs(pageSize, page*pageSize).
		WillReturnRows(
			sqlmock.NewRows(columns).
				FromCSVString(convertObjectToCSV(item1)).
				FromCSVString(convertObjectToCSV(item2)).
				FromCSVString(convertObjectToCSV(item3)).
				FromCSVString(convertObjectToCSV(item4)).
				FromCSVString(convertObjectToCSV(item5)),
		).
		RowsWillBeClosed()

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.GetProducts(ctx, page, pageSize)
	if err != nil {
		t.Fatalf("Error '%s' was not expected when fetching product items", err)
	}

	if len(result) != pageSize {
		t.Fatalf("Unexpected number of items were given, '%d'. Expected '%d'.", len(result), pageSize)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_GetProducts_WhenErrorOccurs_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	const pageSize = 5
	const page = 0
	error := createError()

	mock.ExpectQuery("SELECT id, name, price, manufacturer FROM product ORDER BY id LIMIT \\$1 OFFSET \\$2").
		WithArgs(pageSize, page*pageSize).
		WillReturnError(error)

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.GetProducts(ctx, page, pageSize)
	if result != nil {
		t.Fatalf("Result '%s' was not expected when simulating a failed fetching product item", err)
	}

	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating a failed fetching product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_GetProductByID_WhenProductExists_ShouldReturnProduct(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	columns := []string{"id", "name", "price", "manufacturer"}
	item1 := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}

	mock.ExpectQuery("SELECT id, name, price, manufacturer FROM product WHERE id = \\$1").
		WithArgs(item1.ID).
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString(convertObjectToCSV(item1))).
		RowsWillBeClosed()

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.GetProductByID(ctx, item1.ID)
	if err != nil {
		t.Fatalf("Error '%s' was not expected when fetching product item", err)
	}

	if result != item1 {
		t.Fatalf("Unexpected item was given, '%+v'. Expected '%+v'.", result, item1)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_GetProductByID_WhenProductDoesNotExist_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1ID := int64(1)
	error := createError()

	mock.ExpectQuery("SELECT id, name, price, manufacturer FROM product WHERE id = \\$1").
		WithArgs(item1ID).
		WillReturnError(error)

	sut := NewRepository(dbConn)
	ctx := context.Background()

	_, err = sut.GetProductByID(ctx, item1ID)
	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating a failed fetching product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_GetProductByID_WhenErrorOccurs_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1ID := int64(1)
	error := createError()

	mock.ExpectQuery("SELECT id, name, price, manufacturer FROM product WHERE id = \\$1").
		WithArgs(item1ID).
		WillReturnError(error)

	sut := NewRepository(dbConn)
	ctx := context.Background()

	_, err = sut.GetProductByID(ctx, item1ID)

	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating a failed fetching product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_AddProduct_ShouldReturnInsertedProduct(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	columns := []string{"id"}
	item := ProductItem{ID: 0, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}

	mock.ExpectQuery("INSERT INTO product \\(name, price, manufacturer\\) VALUES \\(\\$1, \\$2, \\$3\\)").
		WithArgs(item.Name, item.Price, item.Manufacturer).
		WillReturnRows(sqlmock.NewRows(columns).FromCSVString(fmt.Sprintf("%+8d", item.ID)))

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.AddProduct(ctx, &item)
	if err != nil {
		t.Fatalf("Error '%s' was not expected when adding an item to product", err)
	}

	if result != item {
		t.Fatalf("Unexpected item was given, '%+v'. Expected '%+v'.", result, item)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_AddProduct_WhenErrorOccurs_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}
	error := createError()

	mock.ExpectQuery("INSERT INTO product \\(name, price, manufacturer\\) VALUES \\(\\$1, \\$2, \\$3\\)").
		WithArgs(item.Name, item.Price, item.Manufacturer).
		WillReturnError(error)

	sut := NewRepository(dbConn)
	ctx := context.Background()

	_, err = sut.AddProduct(ctx, &item)
	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating failure while adding product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_UpdateProduct_ShouldUpdateSpecificProduct(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1 := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE product SET name = \\$1, price = \\$2, manufacturer = \\$3 WHERE id = \\$4").
		WithArgs(item1.Name, item1.Price, item1.Manufacturer, item1.ID).
		WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectCommit()

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.UpdateProduct(ctx, &item1)
	if err != nil {
		t.Fatalf("Result '%s' was not expected when simulating failure while updating product item", err)
	}

	if result != item1 {
		t.Fatalf("Unexpected item was given, '%+v'. Expected '%+v'.", result, item1)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_UpdateProduct_WhenErrorOccurs_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1 := ProductItem{ID: 1, Name: fake.ProductName(), Price: 23, Manufacturer: fake.Brand()}
	error := createError()

	mock.ExpectBegin()
	mock.ExpectExec("UPDATE product SET name = \\$1, price = \\$2, manufacturer = \\$3 WHERE id = \\$4").
		WithArgs(item1.Name, item1.Price, item1.Manufacturer, item1.ID).
		WillReturnError(error)
	mock.ExpectRollback()

	sut := NewRepository(dbConn)
	ctx := context.Background()

	_, err = sut.UpdateProduct(ctx, &item1)
	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating failure while updating product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_RemoveProduct_ShouldReturnID(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1ID := int64(1)

	mock.ExpectPrepare("DELETE FROM product WHERE id = \\$1").
		ExpectExec().
		WithArgs(item1ID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.RemoveProduct(ctx, item1ID)
	if err != nil {
		t.Fatalf("Result '%s' was not expected when simulating failure while removing product item", err)
	}

	if result != item1ID {
		t.Fatalf("Unexpected id was given, '%d'. Expected '%d'.", result, item1ID)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func Test_ProductRepository_RemoveProduct_WhenErrorOccurs_ShouldReturnError(t *testing.T) {
	dbConn, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer dbConn.Close()

	item1ID := int64(1)
	error := createError()

	mock.ExpectPrepare("DELETE FROM product WHERE id = \\$1").
		ExpectExec().
		WithArgs(item1ID).
		WillReturnError(error)

	sut := NewRepository(dbConn)
	ctx := context.Background()

	result, err := sut.RemoveProduct(ctx, item1ID)
	if result != item1ID {
		t.Fatalf("Unexpected id was given, '%d'. Expected '%d'.", result, item1ID)
	}

	if error != err {
		t.Fatalf("Expected failure '%s', but received '%s' when simulating failure while removing product item", error, err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func convertObjectToCSV(item ProductItem) string {
	return fmt.Sprintf("%d,%s,%d,%s", item.ID, item.Name, item.Price, item.Manufacturer)
}

func createError() error {
	return fmt.Errorf("some error")
}
