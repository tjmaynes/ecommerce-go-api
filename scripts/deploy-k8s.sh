#!/bin/bash

set -e

LOCATION=$1

function check_requirements() {
  if [[ -z "$(command -v kubectl)" ]]; then
    echo "Please install 'kubectl' before running this script"
    exit 1
  fi
}

function main() {
  check_requirements

  kubectl apply -f ./k8s/common/secret.yml
	kubectl apply -f ./k8s/db/deployment.yml
	kubectl apply -f ./k8s/db/persistence.$LOCATION.yml
}

main
