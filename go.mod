module github.com/tjmaynes/ecommerce-go-api

go 1.18

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-chi/chi v1.5.4
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/lib/pq v1.10.5
)

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/corpix/uarand v0.1.1 // indirect
)
