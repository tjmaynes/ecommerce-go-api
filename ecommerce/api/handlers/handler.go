package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

// Initialize ..
func Initialize(productHandler *ProductHandler, healthCheckHandler *HealthCheckHandler) http.Handler {
	router := chi.NewRouter()
	router.Use(
		middleware.Recoverer,
		middleware.Logger,
	)

	router.Route("/", func(rt chi.Router) {
		rt.Get("/health", healthCheckHandler.GetHealthCheckHandler)
	})

	router.Route("/api/v1", func(rt chi.Router) {
		rt.Mount("/products", addProductRouter(productHandler))
	})

	return router
}

func addProductRouter(productHandler *ProductHandler) http.Handler {
	router := chi.NewRouter()

	router.Get("/", productHandler.GetProducts)
	router.Get("/{id:[0-9]+}", productHandler.GetProductByID)
	router.Post("/", productHandler.AddProduct)
	router.Put("/{id:[0-9]+}", productHandler.UpdateProduct)
	router.Delete("/{id:[0-9]+}", productHandler.RemoveProduct)

	return router
}
