package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	jsonHandler "github.com/tjmaynes/ecommerce-go-api/internal/shared/helpers"
	"github.com/tjmaynes/ecommerce-go-api/internal/shared/product"
)

// NewProductHandler ..
func NewProductHandler(service product.Service) *ProductHandler {
	return &ProductHandler{Service: service}
}

// ProductHandler ..
type ProductHandler struct {
	Service product.Service
}

// GetProductItems ..
func (c *ProductHandler) GetProducts(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	page, err := strconv.ParseInt(r.URL.Query().Get("page"), 10, 64)
	if err != nil {
		page = 0
	}

	pageSize, err := strconv.ParseInt(r.URL.Query().Get("pageSize"), 10, 64)
	if err != nil {
		pageSize = 10
	}

	data, err := c.Service.GetProducts(r.Context(), page, pageSize)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonHandler.CreateResponse(w, http.StatusOK, map[string][]product.ProductItem{"data": data})
}

// GetProductItemByID ..
func (c *ProductHandler) GetProductByID(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	id, errorCode := getID(r.URL.Path)
	if errorCode >= 400 {
		http.Error(w, http.StatusText(errorCode), errorCode)
	}

	data, err := c.Service.GetProductByID(r.Context(), id)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}

	jsonHandler.CreateResponse(w, http.StatusOK, map[string]product.ProductItem{"data": data})
}

// AddProductItem ..
func (c *ProductHandler) AddProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	r.ParseForm()

	itemName := r.Form.Get("name")
	itemManufacturer := r.Form.Get("manufacturer")
	itemPrice, errorCode := getItemPrice(r.Form.Get("price"))
	if errorCode >= 400 {
		http.Error(w, http.StatusText(errorCode), errorCode)
		return
	}

	data, err := c.Service.AddProduct(r.Context(), itemName, product.Decimal(itemPrice), itemManufacturer)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	jsonHandler.CreateResponse(w, http.StatusCreated, map[string]product.ProductItem{"data": data})
}

// UpdateProductItem ..
func (c *ProductHandler) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method != "PUT" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	id, errorCode := getID(r.URL.Path)
	if errorCode >= 400 {
		http.Error(w, http.StatusText(errorCode), errorCode)
		return
	}

	decoder := json.NewDecoder(r.Body)
	type itemRequest struct {
		Name         string `json:"name"`
		Price        string `json:"price"`
		Manufacturer string `json:"manufacturer"`
	}
	var item itemRequest
	err := decoder.Decode(&item)
	if err != nil {
		panic(err)
	}

	itemPrice, errorCode := getItemPrice(item.Price)
	if errorCode >= 400 {
		http.Error(w, "Price value is invalid.", errorCode)
		return
	}

	result, err := c.Service.GetProductByID(r.Context(), id)
	if err != nil {
		http.Error(w, http.StatusText(404), 404)
		return
	}

	if id != result.ID {
		http.Error(w, http.StatusText(http.StatusNoContent), http.StatusNoContent)
		return
	}

	data, serviceError := c.Service.UpdateProduct(r.Context(), id, item.Name, product.Decimal(itemPrice), item.Manufacturer)
	if serviceError != nil {
		switch serviceError.StatusCode() {
		case product.InvalidItem:
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		default:
			http.Error(w, http.StatusText(500), 500)
		}
		return
	}

	jsonHandler.CreateResponse(w, http.StatusOK, map[string]product.ProductItem{"data": data})
}

// RemoveProductItem ..
func (c *ProductHandler) RemoveProduct(w http.ResponseWriter, r *http.Request) {
	if r.Method != "DELETE" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	id, errorCode := getID(r.URL.Path)
	if errorCode >= 400 {
		http.Error(w, http.StatusText(errorCode), errorCode)
	}

	_, serviceError := c.Service.RemoveProduct(r.Context(), id)
	if serviceError != nil {
		http.Error(w, serviceError.Message(), 500)
		return
	}

	jsonHandler.CreateResponse(w, http.StatusOK, http.StatusText(200))
}

func getID(urlPath string) (int64, int) {
	params := strings.Split(urlPath, "/")
	if len(params) < 2 {
		return 0, http.StatusBadRequest
	}

	id, err := strconv.ParseInt(params[2], 10, 64)
	if err != nil {
		return 0, http.StatusBadRequest
	}

	return id, 0
}

func getItemPrice(rawPrice string) (int64, int) {
	result, err := strconv.ParseInt(rawPrice, 10, 64)
	if err != nil {
		return 0, http.StatusBadRequest
	}
	return result, 0
}
