package api

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/icrowley/fake"
	"github.com/tjmaynes/ecommerce-go-api/internal/shared/helpers"
	"github.com/tjmaynes/ecommerce-go-api/internal/shared/product"
)

var (
	dbConnectionString = flag.String("DATABASE_URL", os.Getenv("DATABASE_URL"), "Database source such as ./db/my.db.")
	dbConn             = getDbConn()
)

func Test_HealthCheckEndpoint_WhenDatabaseConnectionIsAlive_ReturnsPong(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	request, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusOK != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
	}

	if body := recorder.Body.String(); body != `{"message":"PONG!"}` {
		t.Errorf("Expected a PONG! message. Got %s", body)
	}
}

func Test_ProductEndpoint_WhenUnsupportedMethodIsGiven_Returns405(t *testing.T) {
	flag.Parse()

	var tests = []struct {
		httpMethod string
		endpoint   string
	}{
		{"PUT", "/api/v1/products"},
		{"POST", "/api/v1/products/123"},
	}

	for _, tt := range tests {
		a := NewAPI(*dbConnectionString)

		request, err := http.NewRequest(tt.httpMethod, tt.endpoint, nil)
		if err != nil {
			t.Fatal(err)
		}

		recorder := httptest.NewRecorder()
		a.Handler.ServeHTTP(recorder, request)

		if http.StatusMethodNotAllowed != recorder.Code {
			t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
		}
	}
}

func Test_ProductEndpoint_GetProducts_WhenItemsExist_ShouldReturnAllItems(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	ctx := context.Background()
	productRepository := product.NewRepository(dbConn)
	setupDatabase(ctx, productRepository)

	pageSize := int64(5)
	page := int64(0)

	requestURL := fmt.Sprintf("/api/v1/products?page=%d&pageSize=%d", 0, 5)
	request, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusOK != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
	}

	expectedItems, _ := productRepository.GetProducts(ctx, page, pageSize)
	expected := createResponseBody(expectedItems)

	if body := recorder.Body.String(); body != expected {
		t.Errorf("Expected an array of product items. Got %s instead of %s", body, expected)
	}

	teardownDatabase(ctx)
}

func Test_ProductEndpoint_GetProductByID_WhenItemExists_ShouldReturnItem(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	ctx := context.Background()
	productRepository := product.NewRepository(dbConn)
	items := setupDatabase(ctx, productRepository)

	item1 := items[0]
	requestURL := fmt.Sprintf("/api/v1/products/%d", item1.ID)

	request, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusOK != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
	}

	expected := createResponseBody(item1)

	if body := recorder.Body.String(); body != expected {
		t.Errorf("Expected a product. Got %s", body)
	}

	teardownDatabase(ctx)
}

func Test_ProductEndpoint_GetProductByID_WhenItemDoesNotExist_ShouldReturn404(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	request, err := http.NewRequest("GET", "/api/v1/products/-1", nil)
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusNotFound != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusNotFound, recorder.Code)
	}
}

func Test_ProductEndpoint_AddItem_WhenGivenValidItem_ShouldReturnItem(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	ctx := context.Background()
	productRepository := product.NewRepository(dbConn)

	setupDatabase(ctx, productRepository)

	itemName := fake.ProductName()
	itemPrice := product.Decimal(99)
	itemManufacturer := fake.Brand()
	newItem := product.ProductItem{Name: itemName, Price: itemPrice, Manufacturer: itemManufacturer}

	form := url.Values{}
	form.Add("name", newItem.Name)
	form.Add("price", fmt.Sprintf("%d", newItem.Price))
	form.Add("manufacturer", newItem.Manufacturer)

	request, err := http.NewRequest("POST", "/api/v1/products", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusCreated != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusCreated, recorder.Code)
	}

	var result struct {
		Data product.ProductItem `json:"data"`
	}

	err = json.Unmarshal([]byte(recorder.Body.String()), &result)
	if err != nil {
		t.Fatal(err)
	}

	newItem.ID = result.Data.ID

	if result.Data != newItem {
		t.Errorf("Expected a product item %+v. Got %+v", newItem, result.Data)
	}

	teardownDatabase(ctx)
}

func Test_ProductEndpoint_AddItem_WhenGivenInvalidItem_ShouldReturnBadRequest(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	form := url.Values{}
	form.Add("name", "")
	form.Add("price", "")
	form.Add("manufacturer", "")

	request, err := http.NewRequest("POST", "/api/v1/products", strings.NewReader(form.Encode()))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusBadRequest != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusBadRequest, recorder.Code)
	}
}

func Test_ProductEndpoint_UpdateProduct_WhenGivenValidItemAndItemExists_ShouldReturnUpdatedItem(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	ctx := context.Background()
	productRepository := product.NewRepository(dbConn)
	items := setupDatabase(ctx, productRepository)

	newItem := items[0]

	jsonRequest, _ := json.Marshal(map[string]string{
		"name":         newItem.Name,
		"price":        fmt.Sprintf("%d", newItem.Price),
		"manufacturer": newItem.Manufacturer,
	})

	requestURL := fmt.Sprintf("/api/v1/products/%d", newItem.ID)

	request, err := http.NewRequest("PUT", requestURL, bytes.NewReader(jsonRequest))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusOK != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
	}

	var result struct {
		Data product.ProductItem `json:"data"`
	}
	err = json.Unmarshal([]byte(recorder.Body.String()), &result)
	if err != nil {
		t.Fatal(err)
	}

	if result.Data != newItem {
		t.Errorf("Expected a product item %+v. Got %+v", newItem, result.Data)
	}

	teardownDatabase(ctx)
}

func Test_ProductEndpoint_UpdateProduct_WhenGivenValidItemAndItemDoesNotExist_ShouldReturnUpdatedItem(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	unknownItem := product.ProductItem{
		ID:           2,
		Name:         "Random Item",
		Price:        12000,
		Manufacturer: "Random Manufacturer",
	}

	jsonRequest, _ := json.Marshal(map[string]string{
		"name":         unknownItem.Name,
		"price":        fmt.Sprintf("%d", unknownItem.Price),
		"manufacturer": unknownItem.Manufacturer,
	})

	requestURL := fmt.Sprintf("/api/v1/products/%d", unknownItem.ID)

	request, err := http.NewRequest("PUT", requestURL, bytes.NewReader(jsonRequest))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusNotFound != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusNotFound, recorder.Code)
	}
}

func Test_ProductEndpoint_UpdateProduct_WhenGivenInvalidItem_ShouldReturnBadRequest(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	jsonRequest, _ := json.Marshal(map[string]string{
		"name":         "",
		"price":        "",
		"manufacturer": "",
	})

	request, err := http.NewRequest("PUT", "/api/v1/products/0", bytes.NewReader(jsonRequest))
	if err != nil {
		t.Fatal(err)
	}
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusBadRequest != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusBadRequest, recorder.Code)
	}
}

func Test_ProductEndpoint_RemoveProduct_WhenProductExists_ShouldReturnOkResponse(t *testing.T) {
	flag.Parse()

	a := NewAPI(*dbConnectionString)

	ctx := context.Background()
	productRepository := product.NewRepository(dbConn)
	items := setupDatabase(ctx, productRepository)

	newItem := items[1]

	requestURL := fmt.Sprintf("/api/v1/products/%d", newItem.ID)

	request, err := http.NewRequest("DELETE", requestURL, nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	a.Handler.ServeHTTP(recorder, request)

	if http.StatusOK != recorder.Code {
		t.Errorf("Expected response code %d. Got %d\n", http.StatusOK, recorder.Code)
	}

	teardownDatabase(ctx)
}

func createResponseBody(items interface{}) string {
	out, err := json.Marshal(items)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf(`{"data":%s}`, out)
}

func getDbConn() *sql.DB {
	dbConn, err := helpers.ConnectDB(*dbConnectionString)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	return dbConn
}

func generateProducts(itemCount int, manufacturerCount int) []product.ProductItem {
	var manufacturers []string
	for i := 0; i < manufacturerCount; i++ {
		manufacturers = append(manufacturers, fake.Brand())
	}

	var items []product.ProductItem
	for i := 0; i < itemCount; i++ {
		items = append(items, product.ProductItem{
			Name:         fake.ProductName(),
			Price:        product.Decimal(int64(rand.Intn(100) + 100)),
			Manufacturer: manufacturers[rand.Intn(4)],
		})
	}

	return items
}

func setupDatabase(ctx context.Context, productRepository product.Repository) []product.ProductItem {
	var rawItems = generateProducts(100, 5)

	createDatabase(ctx)

	var items []product.ProductItem
	for _, rawItem := range rawItems {
		item, err := productRepository.AddProduct(ctx, &rawItem)
		if err != nil {
			panic(err)
		}
		items = append(items, item)
	}

	return items
}

func createDatabase(ctx context.Context) {
	stmt, err := dbConn.PrepareContext(ctx, `
		CREATE TABLE product (
			id SERIAL PRIMARY KEY,
			name VARCHAR (255) NOT NULL,
			price BIGINT NOT NULL,
			manufacturer VARCHAR (255) NOT NULL
		)
	`)

	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx)
	if err != nil {
		return
	}
}

func teardownDatabase(ctx context.Context) {
	stmt, err := dbConn.PrepareContext(ctx, "DROP TABLE product")
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx)
	if err != nil {
		return
	}
}
