package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"math/rand"
	"os"

	"github.com/icrowley/fake"

	"github.com/tjmaynes/ecommerce-go-api/internal/shared/helpers"
	"github.com/tjmaynes/ecommerce-go-api/internal/shared/product"
)

// SeedData ..
func seedData(dbConn *sql.DB, items []product.ProductItem) []int64 {
	productRepository := product.NewRepository(dbConn)
	ctx := context.Background()
	productService := product.NewService(productRepository)

	var ids []int64
	for _, rawItem := range items {
		item, err := productService.AddProduct(ctx, rawItem.Name, rawItem.Price, rawItem.Manufacturer)
		if err != nil {
			panic(err)
		}
		ids = append(ids, item.ID)
	}

	return ids
}

func generateProducts(itemCount int, manufacturerCount int) []product.ProductItem {
	var manufacturers []string
	for i := 0; i < manufacturerCount; i++ {
		manufacturers = append(manufacturers, fake.Brand())
	}

	var items []product.ProductItem
	for i := 0; i < itemCount; i++ {
		items = append(items, product.ProductItem{
			Name:         fake.ProductName(),
			Price:        product.Decimal(int64(rand.Intn(100) + 100)),
			Manufacturer: manufacturers[rand.Intn(4)],
		})
	}

	return items
}

func main() {
	var (
		dbSource          = flag.String("db-source", "./db/my.db", "Database url connection string.")
		itemCount         = flag.Int("item-count", 50, "Number of seed items, such as 10, 20, 50, etc.")
		manufacturerCount = flag.Int("manufacturer-count", 5, "Number of unique manufacturers, such as 10, 20, 50, etc.")
	)

	flag.Parse()

	dbConn, err := helpers.ConnectDB(*dbSource)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	ids := seedData(dbConn, generateProducts(*itemCount, *manufacturerCount))
	fmt.Printf("ADDED %d products.", len(ids))
}
